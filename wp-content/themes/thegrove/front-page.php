<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 */

get_header();

?>

<div data-scroll-container>

    <div class="the-grove-banner" data-scroll-section>
    
        <div class="the-grove-banner__initial"></div>
        <div class="the-grove-banner__first"></div>
        <div class="the-grove-banner__second"></div>
        <div class="the-grove-banner__third"></div>
    
        <div class="the-grove-banner__text the-grove-banner__text--first">
            <div class="the-grove-banner__content">
                <div class="the-grove-banner__the-grove mb-1">
                    <h1 class="u-text-center text-intro-animation">THE GROVE</h1>
                </div>
                <div class="the-grove-banner__building-icon">
                    <?php include get_icons_directory('House-icon.svg') ?>
                </div>
            </div>
        </div>
    
        <div class="the-grove-banner__text the-grove-banner__text--second">
            <div class="the-grove-banner__content">
                <div class="the-grove-banner__the-grove mb-1">
                    <h1 class="u-text-center text-intro-animation">THE GROVE</h1>
                </div>
                <div class="the-grove-banner__building-icon">
                    <?php include get_icons_directory('House-icon.svg') ?>
                </div>
            </div>
        </div>
    
        <div class="the-grove-banner__text__final" id="fixed-target">
            <div class="the-grove-banner__text--third the-grove-banner__flex-container" data-scroll data-scroll-repeat data-scroll-call="pageColor" data-scroll-id="#535D58">
                <div class="the-grove-banner__logo fixed" data-scroll data-scroll-sticky data-scroll-target="#fixed-target">
                    <div class="the-grove-banner__the-grove-final mb-1">
                        <h1 class="u-text-center text-homepage-banner">THE GROVE</h1>
                    </div>
                    <h2 class="the-grove-banner__subtitle text-header text-subheader">CONDOS IN LESLIEVILLE</h2>
                </div>
            </div>
            <?php

            $firstRImage = get_field("first_right_image") ? get_field("first_right_image") : "";
            $LImage = get_field("left_image") ? get_field("left_image") : "";
            $secondRImage = get_field("second_right_image") ? get_field("second_right_image") : "";

            $FirstRImageMobile = get_field("first_right_image_mobile") ? get_field("first_right_image_mobile") : "";
            $LImageMobile = get_field("left_image_mobile") ? get_field("left_image_mobile") : "";
            $secondRImageMobile = get_field("second_right_image_mobile") ? get_field("second_right_image_mobile") : "";

            ?>
            <div class="the-grove-banner__images">
                <div class="<?=(!wp_is_mobile()) ? "wrapper-full" : "wrapper-stretched" ?>" data-scroll data-scroll-repeat data-scroll-call="pageColor" data-scroll-offset="600" data-scroll-id="#1E2220">
                    <div class="the-grove-banner__paralax the-grove-banner__paralax--right d-flex">
                        <img src="<?=(!wp_is_mobile()) ? $firstRImage['url'] : $FirstRImageMobile['url'] ?>" alt="" data-scroll data-scroll-speed="4">
                    </div>
                    <div class="the-grove-banner__paralax the-grove-banner__paralax--left d-flex">
                        <img src="<?=(!wp_is_mobile()) ? $LImage['url'] : $LImageMobile ['url']?>" alt="" data-scroll data-scroll-speed="4">
                    </div>
                    <div class="the-grove-banner__paralax the-grove-banner__paralax--right-second d-flex">
                        <img src="<?=(!wp_is_mobile()) ? $secondRImage['url'] : $secondRImageMobile['url'] ?>" alt="" data-scroll data-scroll-speed="4">
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    <div class="thegrove__relative" id="fixed-target-logo" data-scroll-section>
        <div>
            <div class="thegrove__logo" data-scroll data-scroll-repeat data-scroll-sticky data-scroll-target="#fixed-target-logo">
                <img class="thegrove__logo__icon" src="<?= (wp_is_mobile()) ? get_theme_root_uri()."/thegrove/src/images/svg/The-Grove-small.svg" : get_theme_root_uri()."/thegrove/src/images/svg/The-Grove.svg" ?>">
            </div>
        
            <?php
            fuzion_layout();
            get_footer();
            ?>

        </div>
    
    </div>
</div>

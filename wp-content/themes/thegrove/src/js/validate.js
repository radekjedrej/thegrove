import $ from "jquery";

// Form Validate
class Validate {
  constructor() {
    this.events();
  }

  events() {
    const form = document.getElementById("mc-embedded-subscribe-form");
    const firstName = document.getElementById("mce-FIRSTNAME");
    const lastName = document.getElementById("mce-LASTNAME");
    const phoneNumber = document.getElementById("mce-MMERGE5");
    const email = document.getElementById("mce-EMAIL");
    const social = document.getElementById("mce-SOCIAL");

    if ($("#mc-embedded-subscribe-form").length > 0) {
      form.addEventListener("submit", e => {
        e.preventDefault();

        checkInputs();

        // Check if any errors
        if ($(".form-control.success").length == $(".form-control").length) {
          form.submit();
        }
      });
    }

    function checkInputs() {
      // trim to remove the whitespaces
      const firstNameValue = firstName.value.trim();
      const lastNameValue = lastName.value.trim();
      const phoneNumberValue = phoneNumber.value.trim();
      const emailValue = email.value.trim();
      const socialValue = social.value.trim();

      if (firstNameValue === "") {
        setErrorFor(firstName, "First name must be filled out");
      } else {
        setSuccessFor(firstName);
      }

      if (lastNameValue === "") {
        setErrorFor(lastName, "Last name must be filled out");
      } else {
        setSuccessFor(lastName);
      }

      if (phoneNumberValue === "") {
        setErrorFor(phoneNumber, "Phone must be filled out");
      } else {
        setSuccessFor(phoneNumber);
      }

      if (emailValue === "") {
        setErrorFor(email, "Mail must be filled out");
      } else if (!isEmail(emailValue)) {
        setErrorFor(
          email,
          "A valid email address is required (ex. email@domain.ca)"
        );
      } else {
        setSuccessFor(email);
      }

      if (socialValue === "") {
        setErrorFor(social, "This field is required.");
      } else {
        setSuccessFor(social);
      }
    }

    function setErrorFor(input, message) {
      const formControl = input.parentElement;

      if (!formControl.classList.contains("warning")) {
        formControl.classList.remove("form-control", "success");
        formControl.classList.add("form-control", "warning");
        formControl.insertAdjacentHTML(
          "beforeend",
          `<div class="error-message">${message}</div>`
        );
      }
    }

    function setSuccessFor(input) {
      const formControl = input.parentElement;

      if (formControl.classList.contains("warning")) {
        formControl.classList.remove("form-control", "warning");
        formControl.classList.add("form-control", "success");
        formControl.querySelector(".error-message").remove();
      }
    }

    function isEmail(email) {
      return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      );
    }
  }
}

export default Validate;

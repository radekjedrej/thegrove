import $ from 'jquery'

class Menu {
    constructor(){
        this.document = $("html")
        this.borders = $(".page-borders")
        this.dark = $(".nav-bar__dark")
        this.navBarContainer = $(".nav-bar__container")
        this.burger = $(".nav-bar__menu-icon")
        this.socials = $(".nav-bar__socials")
        this.menu = $(".main-menu")
        this.link = $(".main-menu__link")
        this.image = $(".main-menu__main-img")
        this.newImage = $(".main-menu__new-img")
        this.imageText = $(".main-menu__text")
        this.events()
    }

    events() {
        this.burger.on("click", this.showMenu.bind(this))
        this.link.on("mouseover", this.linkMouseOver.bind(this))
        this.link.on("mouseout", this.linkMouseOut.bind(this))
    }

    showMenu(e){
        this.dark.toggleClass("disable")
        this.burger.toggleClass("open")
        this.borders.find(".page-border-top").toggleClass("border-hide-mobile")
        this.socials.toggleClass("margin-left")
        this.navBarContainer.toggleClass("items")
        this.menu.toggleClass("active"); 
        this.document.toggleClass("lock-scroll")
    }

    linkMouseOver(e){
        const aLink = $(e.currentTarget).find("a")
        const imageLink = aLink.data("image")
        const imageTitle = aLink.data("title")
        
        this.imageText.html(imageTitle);
        this.newImage.attr("src", imageLink)
        this.newImage.addClass("active")
    }

    linkMouseOut(e){
        const aLink = $(e.currentTarget).find("a")
        const imageLink = aLink.data("image")

        this.image.attr("src", this.newImage.attr("src"), imageLink)
        this.newImage.removeClass("active")
    }   
}

export default Menu
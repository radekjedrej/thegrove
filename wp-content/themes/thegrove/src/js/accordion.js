import $ from 'jquery';

// Handles auto-height transition
export const handleHeight = ( btn ) => {
    const el = btn.nextElementSibling
    const height = el.scrollHeight

    requestAnimationFrame(() => {
        btn.classList.toggle('active')
        el.style.height = !el.style.height
            ? `${height}px`
            : null
    })
}

// Templates :: 
// export const Accordion = (() => {

//   function toggleAccordion() {
//     const button = document.querySelectorAll('.faq__question')

//     if(button) {
//         f
//     }
//   }
// })

class Accordion {
  constructor() {
    this.content = $(".accordion__li__content");
    this.child = $(".accordion__children__li");
    this.events();
  }

  events() {
    this.checkIfHasChild()
    // this.content.on("click", this.toggleAccordion)
  }

  checkIfHasChild() {
    const parent = this.child.parent().parent()
    parent.addClass("has-child")

    parent.on('click', function toggleAccordion(e){
      $(this).find(".heavy__plus").toggleClass('open-state')
      $(this).find(".accordion__children").slideToggle()
    })
  }
}


export default Accordion;
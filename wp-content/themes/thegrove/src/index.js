import "./js/navigation";
import "./js/skip-link-focus-fix";
import Accordion from "./js/accordion";
import Menu from "./js/menu";
import Validate from "./js/validate";
import LazyLoad from "vanilla-lazyload";
import LocomotiveScroll from 'locomotive-scroll';


import $ from "jquery";

import { vh, handleWindow } from "./js/helpers";

window.addEventListener("DOMContentLoaded", () => {
  const banner = $('.the-grove-banner__first')

  new Accordion();
  new Menu();
  new Validate();
  const myLazyLoad = new LazyLoad();

  const scroll = new LocomotiveScroll({
    el: document.querySelector('[data-scroll-container]'),
    smooth: true,
  });

  setTimeout(() => {
    scroll.on('call', (value, way, obj) => {

      if (way === 'enter') {
        switch (value) {
          case "pageColor":
            // get color code from data-scroll-id  assigned to body by obj.id
            document.querySelector('.the-grove-banner__third').style.backgroundColor = obj.id;
            break;
        }
      }

    });
  }, 600);

  if (banner) {
    $(".the-grove-banner__first").animate({ width: '100%' }, 500);
    $(".the-grove-banner__second").delay(800).animate({ width: '100%' }, 500);
    $(".the-grove-banner__text--second").delay(1000).animate({ opacity: 1 }, 500);
    $(".the-grove-banner__third").delay(1400).animate({ height: '100%' }, 800);
    $(".the-grove-banner__text--third").delay(2000).animate({ opacity: 1 }, 500);
    setTimeout(function () { $(".the-grove-banner__images").addClass('the-grove-banner__images--active') }, 2200);
    setTimeout(function () { $("header").addClass("active") }, 2000);
  }

  const form = document.querySelector('#footer__top');

  $('.register-link').on('click', function(e) {
    e.preventDefault();
    scroll.scrollTo(form)
  });

  $('.nav-bar__register-link-mobile').on('click', function(e) {
    e.preventDefault();
    scroll.scrollTo(form)

    setTimeout(function() {
      scroll.scrollTo(form)
    }, 500)
  });
});

handleWindow();

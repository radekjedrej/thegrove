<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package _s
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function _s_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', '_s_body_classes' );

// Handle layout builders
function fuzion_layout( $name = 'partials', $dir = 'partials' ) 
{
    if ( have_rows( $name ) ) :
        while ( have_rows( $name ) ) : the_row();
    
            $builder = str_replace(
                '_', '-', get_row_layout()
            );
    
            get_template_part( "{$dir}/{$builder}" );
    
        endwhile;
    endif;
}

/**
 * Print SVG Directory
 */ 
function get_icons_directory( $file ) 
{
    $dir = '/src/images/svg/';
    return get_stylesheet_directory() . $dir . $file;
}
<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header();
?>

	<div id="primary">
		<main id="main">

			<section>
				<header>
					<h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', '_s' ); ?></h1>
				</header> <!-- .page-header -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

?>
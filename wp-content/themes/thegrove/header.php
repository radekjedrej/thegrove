<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until main content
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://use.typekit.net/hcj5baj.css">
	<?php wp_head(); ?>
</head>

<body>
<!-- HEADER - START -->

	<header class="<?=(is_front_page() ? "" : "active") ?>">
	<div class="nav-bar <?= (!is_front_page()) ? "nav-bar--dark" : "" ?>">
	<div class="nav-bar__menu-position">
		<div class="nav-bar__dark"></div>
		<div class="nav-bar__container wrapper-nav d-flex">
			<a href="#footer__top" data-scroll-to class="nav-bar__register-link-mobile"><div class="nav-bar__register-icon-content">
				<img class="nav-bar__register-icon" src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/app_registration_white_24dp.svg">
				<span class="text-small color-yellow">Register</span>
			</div></a>
			<div class="nav-bar__page-title <?= (is_front_page()) ? "nav-bar__page-title--hidden" : "" ?>"><span class="color-yellow"><?= the_title(); ?></span></div>
			<div class="nav-bar__the-grove <?= (is_front_page()) ? "nav-bar__the-grove--hidden" : "" ?> d-flex">
				<a class="nav-bar__the-grove-link" href="<?= home_url(); ?>"><img class="nav-bar__the-grove-logo" src="<?= (wp_is_mobile()) ? get_theme_root_uri()."/thegrove/src/images/svg/The-Grove-small.svg" : get_theme_root_uri()."/thegrove/src/images/svg/The-Grove.svg" ?>"></a>
			</div>
			<div class="nav-bar__links d-flex">
			<?php if( have_rows('social_icons', 'option') ): ?>

				<div class="nav-bar__socials">
				<?php while(have_rows('social_icons', 'option') ): the_row();

					$icon = !empty(get_sub_field("icon")) ? get_sub_field("icon") : "";

					if(!empty(get_sub_field("social_link"))):
						$social = get_sub_field("social_link");
						$socialLink = $social['url'];

					else:
						$socialLink = "#";

					endif;

				?>
					<?php if($icon): ?>
						<a class="nav-bar__social-link" href="<?= $socialLink ?>"><img class="nav-bar__social-icon" src="<?= $icon['url'] ?>"></a>
					<?php endif; ?>

				<?php endwhile; ?>
				</div>	

			<?php endif; ?>
			
				<div class="nav-bar__register"><a href="#footer__top" data-scroll-to class="color-yellow text-regular register-link link-underline">Register</a></div>
				<div class="nav-bar__menu-icon color-yellow">
					<div class="nav-bar__burger-icon">
						<div class="nav-bar__burger-icon-line"></div>
					</div>
					<div class="nav-bar__x-button"></div>
				</div>

			</div>
		</div>



		<?php

		$firstImage = get_field("first_image", "option") ? get_field("first_image", "option") : "";

		if( have_rows('menu', 'option') ): ?>

		<div class="main-menu">
			<div class="main-menu__container wrapper-medium">
				<div class="main-menu__links-container">
					<div class="main-menu__g">
						<img class="main-menu__g-icon" src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/G-icon.svg">
					</div>
					<ul class="main-menu__links">
		
					<?php while( have_rows('menu', 'option') ): the_row(); 
					
						$title = !empty(get_sub_field("title")) ? get_sub_field("title") : "";
						$link = !empty(get_sub_field("link")) ? get_sub_field("link") : "";
						$image = get_sub_field("image");
						$picture = $image["sizes"]["menu-image"];
					?>
						
						<li class="main-menu__link">
							<a data-image="<?= $picture ?>" data-title="<?= $title ?>" href="<?= $link['url'] ?>">
								<h1 class="text-header"><?= $link['title'] ?></h1>
							</a>
						</li>

					<?php endwhile; ?>
					</ul>
					<div class="main-menu__building">
						<?php include get_icons_directory('House-icon.svg') ?>
					</div>

					<?php if( have_rows('social_icons', 'option') ): ?>

					<div class="main-menu__socials">
					<?php while(have_rows('social_icons', 'option') ): the_row();

						$icon = !empty(get_sub_field("icon")) ? get_sub_field("icon") : "";
						
						if(!empty(get_sub_field("social_link"))):
							$social = get_sub_field("social_link");
							$socialLink = $social['url'];
	
						else:
							$socialLink = "#";
	
						endif;
						
					?>
						<?php if($icon): ?>
							<a href="<?= $socialLink ?>"><img class="main-menu__social-icon" src="<?= $icon['url'] ?>"></a>
						<?php endif; ?>

					<?php endwhile; ?>
					</div>

					<?php endif; ?>
				</div>

				<div class="main-menu__image">
					<div class="main-menu__img-container">
						<div class="main-menu__dark"></div>
						<img class="main-menu__new-img" alt="new">
						<div class="main-menu__text-container"><h1 class="main-menu__text text-banner">LIFE IN BALANCE</h1></div>
						<img class="main-menu__main-img" src="<?= $firstImage['url']; ?>">
					</div>
					<div class="main-menu__info">
						<div class="main-menu__info-left d-flex">
							<img class="mr-6" src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/Groupe-Tyfoon.svg">
							<img src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/Logo.svg">
						</div>
						<div class="main-menu__info-right d-flex">
							<img class="mr-2" src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/House-icon.svg">
							<div class="main-menu__address d-flex"><span class="color-yellow text-regular">Queen St E. Toronto, ON M4M 1K2</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>
	</div>


	</div>

	<div class="page-borders">
		<div class="page-border-top <?=(is_front_page()) ? "" : "page-border-top--hidden" ?>"></div>
		<div class="page-border-left"></div>
		<div class="page-border-right"></div>
		<div class="page-border-bottom"></div>
	</div>

	

	
	
	</header>

<!-- HEADER - END -->


	

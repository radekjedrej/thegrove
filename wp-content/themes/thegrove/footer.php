<?php
/**
 * The template for displaying the footer
*/

?>

<!-- FOOTER - START -->

	<footer class="footer" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
	<?php if ( have_rows('footer', 'option') ):
		while( have_rows('footer', 'option') ): the_row();
		$the_grove = get_sub_field("the_grove_icon") ? get_sub_field("the_grove_icon") : "";

			if($the_grove AND is_front_page()): ?>
        		<div class="footer__the-grove">
            		<img class="footer__the-grove-icon opacity-zero" src="<?= (wp_is_mobile()) ? get_theme_root_uri()."/thegrove/src/images/svg/The-Grove-small.svg" : get_theme_root_uri()."/thegrove/src/images/svg/The-Grove.svg" ?>">
        		</div>
    		<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>

		<div id="footer__top" class="footer__container wrapper-full d-flex">
			<div class="footer__row d-flex d-flex-wrap">
				<div class="footer__form">

					<!-- Begin Mailchimp Signup Form -->
					<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">


					<section class="section__form" id="section__form">
						<div class="container">
							<div class="form__wrapper color-white">
								<h3 class="display-3 text-center text-header--footer" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="50" data-aos-duration="1600" data-aos-once="true">REGISTER TO SECURE YOUR INTEREST</h3>


								<div id="mc_embed_signup">

									<form class="font-secondary" action="https://COM.us7.list-manage.com/subscribe/post?u=ac62d68da183a67cd7f11b0d3&amp;id=59725a3e19" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="" novalidate>

										<div id="mc_embed_signup_scroll">
											<div class="form__row">
												<div class="mc-field-group">
													<label class="text-regular" for="mce-FIRSTNAME" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="250" data-aos-duration="1600" data-aos-once="true">
														<input type="text" value="" name="FIRSTNAME" class="required" id="mce-FIRSTNAME">First Name*
													</label>
												</div>
												<div class="mc-field-group" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="250" data-aos-duration="1600" data-aos-once="true">
													<label class="text-regular" for="mce-LASTNAME">
														<input type="text" value="" name="LASTNAME" class="required" id="mce-LASTNAME">Last Name*
													</label>
												</div>
											</div>
											<div class="form__row">

												<div class="mc-field-group" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="450" data-aos-duration="1600" data-aos-once="true">
													<label class="text-regular" for="mce-MMERGE5">
														<input type="text" name="MMERGE5" class="required" value="" id="mce-MMERGE5">Phone Number*
													</label>
												</div>


												<div class="mc-field-group" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="450" data-aos-duration="1600" data-aos-once="true">
													<label class="text-regular" for="mce-EMAIL">
														<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">Email Address*
													</label>
												</div>
											</div>
											<div class="form__row">
												<div class="text-regular mc-field-group" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="650" data-aos-duration="1600" data-aos-once="true">
													<select name="SOCIAL" class="required" id="mce-SOCIAL">
														<option value=""></option>
														<option value="Broker / Agent">Broker / Agent</option>
														<option value="Buzz Buzz Homes">Buzz Buzz Homes</option>
														<option value="Instagram / Facebook">Instagram / Facebook</option>
														<option value="Google">Google</option>
														<option value="Signage">Signage</option>
														<option value="Word of Mouth">Word of Mouth</option>
													</select>How did you hear about The Grove Condos?*
												</div>

												<div class="clear mc-field-group" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="650" data-aos-duration="1600" data-aos-once="true">
													<input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
													<span class="display-5 text-small">By clicking Submit, you agree, in accordance with Canada’s Anti-Spam Legislation, to recieve digital communications from Presidential Group and affiliated partners.</span>
												</div>

												<div id="mce-responses" class="clear">
													<div class="response" id="mce-error-response" style="display:none"></div>
													<div class="response" id="mce-success-response" style="display:none"></div>
												</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
												<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ac62d68da183a67cd7f11b0d3_59725a3e19" tabindex="-1" value=""></div>

											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</section>

				<!--End mc_embed_signup-->
				</div>


				<div class="footer__info d-flex">
					<section class="section__info">
						<div class="footer__email">
							<h4 class="text-subheader text-regular mt-1 mb-1 color-sage-green">SCHEDULE A PRIVATE SALES APPOINTMENT</h4>
							<div class="footer__email-content mb-4"><span class="text-regular color-yellow">janet@alsinclair.com</span></div>
						</div>
						<div class="footer__address mb-5">
							<h4 class="text-subheader text-regular mt-1 mb-1 color-sage-green">SALES OFFICE</h4>
							<div class="footer__address-content"><span class="text-regular color-yellow">1001 Queen St E., Toronto, ON M4M 1K2 Open by appointment only</span></div>
						</div>
						<div class="footer__map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1443.1747116424376!2d-79.3388665!3d43.6617022!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d4cb7656869cb3%3A0x5d3be4bcc66161cf!2s1001%20Queen%20St%20E%2C%20Toronto%2C%20ON%20M4M%201K2%2C%20Kanada!5e0!3m2!1spl!2spl!4v1617101368620!5m2!1spl!2spl" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe> 
						</div>
					</section>
				</div>
			</div>

			<div class="footer__bottom-row d-flex d-flex-wrap">
				<div class="footer__image-row d-flex">
					<div>
						<img src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/Groupe-Tyfoon.svg">
					</div>
					<div>
						<img src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/Logo.svg">
					</div>
				</div>

				<div class="footer__link-row d-flex">
					<div class="footer__link-svg d-desktop"><?php include get_icons_directory('House-icon.svg') ?></div>
					<div class="footer__link-content d-flex">
						<div class="footer__link-text">
							<span class="text-small color-yellow">Furniture is displayed for illustration purposes only and suites are sold unfurnished. Building finishes and views may vary from those shown. Building and view not to scale. E. & O.E.</span>
						</div>

						<div>
							<span class="text-small color-yellow">Branding & Web by <a class="link-underline" href="https://www.channel13.ca/">Channel 13</a></span>
						</div>

					</div>
				</div>
			</div>
		</div>
	</footer>

<!-- FOOTER - END -->
<?php wp_footer(); ?>

</body>
</html>

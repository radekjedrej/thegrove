<?php

$text_box_repeater = !empty(get_sub_field("text_box_repeater")) ? get_sub_field("text_box_repeater") : "";

?>

<?php if($text_box_repeater): ?>

<section class="the-team-row" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
    <div class="the-team-row__container wrapper-full">
    <?php if(have_rows("text_box_repeater")): ?>

        <div class="the-team-row__text-container d-flex d-flex-wrap">
        <?php while(have_rows("text_box_repeater")): the_row();
            
            $title = !empty(get_sub_field("title")) ? get_sub_field("title") : "";
            $title_mobile = !empty(get_sub_field("title_mobile")) ? get_sub_field("title_mobile") : "";

            $subtitle_mobile = !empty(get_sub_field("subtitle_only_mobile")) ? get_sub_field("subtitle_only_mobile") : "";
            
            $text = !empty(get_sub_field("text")) ? get_sub_field("text") : "";
    ?>
            <div class="the-team-row__text-box">
                <div class="the-team-row__content">
                    
                    <?php if($title AND $title_mobile): ?>
                        <h2 class="the-team-row__title text-regular text-subheader"><?=(!wp_is_mobile()) ? $title : $title_mobile ?></h2>
                    <?php endif; ?>

                    <?php if($subtitle_mobile): ?>
                        <h2 class="the-team-row__subtitle text-regular text-subheader mt-1"><?=(wp_is_mobile()) ? $subtitle_mobile : "" ?></h2>
                    <?php endif; ?>
                        
                    <?php if($text): ?>
                        <p class="the-team-row__text text-regular mt-2"><?= $text ?></p>
                    <?php endif; ?>

                </div>
            </div>
        <?php
            endwhile;
        ?>
        </div>

        <?php
        endif; 
        ?>
    </div>
</section>
<?php endif; ?>
<?php

    if(get_sub_field('image')):
        $image = get_sub_field("image");
        $picture = $image["sizes"]["side-image"];

    endif;
    
    if(get_sub_field('image_mobile')):
        $mobile_image = get_sub_field("image_mobile");
        $mobile_picture = $mobile_image["url"];

    endif;

    $title = get_sub_field("title") ? get_sub_field("title") : "";
    $subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
    $text = get_sub_field("text") ? get_sub_field("text") : "";
    $background = get_sub_field("background_select");
    
    $svg = get_sub_field("svg") ? get_sub_field("svg") : "";

?>

    <section class="side-image-row wrapper-stretched <?= $background; ?>" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>

        <div class="wrapper-xfull side-image-row__container">
            <div class="side-image-row__row d-flex d-flex-wrap">
                <div class="side-image-row__image-box">
                    <div class="side-image-row__image img-fluid">
                        <img class="side-image-row__img img-fluid lazy" data-src="<?= (wp_is_mobile()) ? $mobile_picture : $picture ?>">
                    </div>
                </div>
                            
                <div class="side-image-row__content-box d-flex">
                    <div class="side-image-row__inner">
                        <div class="side-image-row__content u-text-center">
                            <h1 class="side-image-row__content-title text-header"><?= $title; ?></h1>
                            
                            <?php if($subtitle): ?>
                                <h2 class="side-image-row__content-subtitle text-regular text-subheader mt-2"><?= $subtitle; ?></h2>
                            <?php endif; ?>

                            <?php if($text): ?>
                                <p class="side-image-row__text text-regular mt-5 mb-4 mb-lg-0"><?= $text; ?></p>
                            <?php endif; ?>

                            <?php if($svg): ?>
                                <div class="side-image-row__content-image">
                                    <img src="<?= $svg["sizes"]["medium"]; ?>" class="side-image-row__content-img">
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
            

<?php

    $title = get_sub_field("title") ? get_sub_field("title") : "";
    $subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
    $text = get_sub_field("text") ? get_sub_field("text") : "";
    $background = get_sub_field("background_select");

?>

<section class="text-row <?= $background; ?> wrapper-stretched" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
    <div class="text-row__container wrapper-full d-flex">
        <div class="text-row__inner d-flex">

            <div class="text-row__content">
                <?php if($title): ?>
                    <h1 class="text-row__header text-header"><?= $title; ?></h1>
                <?php endif; ?>
            
                <?php if($subtitle): ?>
                    <h2 class="text-row__subheader text-regular text-subheader"><?= $subtitle; ?></h2>
                <?php endif; ?>
            
                <?php if($text): ?>
                    <p class="text-row__text text-regular"><?= $text ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
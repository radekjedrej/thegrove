<?php

$space_mobile = get_sub_field('space_on_mobile_in_pixels');
$space_desktop = get_sub_field('space_on_desktop_in_pixels');
$background = get_sub_field('background_select');

?>

<div class="spacing-row space-<?=(!wp_is_mobile()) ? $space_desktop : $space_mobile ?> <?= $background ?>" <?= !is_front_page() ? 'data-scroll-section' : '' ?>></div>

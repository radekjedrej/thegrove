<?php

    $title = get_sub_field("title") ? get_sub_field("title") : "";
    $subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
    $text = get_sub_field("text") ? get_sub_field("text") : "";

    if(get_sub_field("image")):
        $image = get_sub_field("image");
        $picture = $image["sizes"]["stretched-image"];

    endif;

    if(get_sub_field("image_mobile")): 
        $mobile_image = get_sub_field("image_mobile");
        $mobile_picture = $mobile_image['url'];

    endif;
    
    $background = get_sub_field("background_select");
    $side = get_sub_field("image_side");
    $mobile_side = get_sub_field("image_side_mobile");

?>


<section class="stretched-image-row <?= $background; ?> <?= $side; ?> wrapper-stretched" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
    <div class="stretched-image-row__mobile-order <?= $mobile_side; ?>">
        <div class="stretched-image-row__image">
            <img class="stretched-image-row__img img-fluid lazy" data-src="<?= (wp_is_mobile()) ? $mobile_picture : $picture ?>">   
        </div>
        <div class="stretched-image-row__container wrapper-full d-flex">
            <div class="stretched-image-row__content-box">
                <div class="stretched-image-row__content">
                    <h1 class="stretched-image-row__content-title text-header mb-3 mb-lg-2 mt-8 mt-lg-0"><?= $title; ?></h2>
                    <h2 class="stretched-image-row__content-subtitle text-regular text-subheader mb-4 mb-lg-5"><?= $subtitle; ?></h2>
                    <p class="stretched-image-row__text text-regular mb-8 mb-lg-0"><?= $text; ?></p>
                </div>
            </div>
        </div>  
    </div>
</section>
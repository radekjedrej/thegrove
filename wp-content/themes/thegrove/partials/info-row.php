<?php

    $icons = get_sub_field("icons_row") ? get_sub_field("icons_row") : "";
    $title = get_sub_field("title") ? get_sub_field("title") : "";
    $subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
    $text = get_sub_field("text") ? get_sub_field("text") : "";
    $textContainerWidth = get_sub_field("text_container_max_width") ? get_sub_field("text_container_max_width") : "";
    $building_icon = get_sub_field("the_building_icon") ? get_sub_field("the_building_icon") : "";
    $background = get_sub_field("background_select") ? get_sub_field("background_select") : "";

?>

<section class="info-row <?= $background ?> wrapper-stretched" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
    <div class="info-row__container wrapper-full <?=($building_icon) ? "info-row__container--space-between" : "" ?> d-flex">
         <div class="info-row__the-grove">
            <img class="info-row__the-grove-icon opacity-zero" src="<?= (wp_is_mobile()) ? get_theme_root_uri()."/thegrove/src/images/svg/The-Grove-small.svg" : get_theme_root_uri()."/thegrove/src/images/svg/The-Grove.svg" ?>">
        </div>

        <div class="info-row__inner d-flex">
        <?php if($icons): ?>
            <div class="info-row__icons mb-4 mb-lg-5 d-flex">
                <div class="info-row__icon info-row__icon--gt mb-1"><?php include get_icons_directory('Groupe-Tyfoon.svg') ?></div>
                <div class="info-row__icon info-row__icon--logo"><?php include get_icons_directory('Logo.svg') ?></div>
            </div>
        <?php endif; ?>

            <div class="info-row__content <?= $textContainerWidth ?>">
                <?php if($title): ?>
                    <h1 class="info-row__header text-header"><?= $title; ?></h1>
                <?php endif; ?>
            
                <?php if($subtitle): ?>
                    <h2 class="info-row__subheader text-regular text-subheader"><?= $subtitle; ?></h2>
                <?php endif; ?>
            
                <?php if($text): ?>
                    <p class="info-row__text text-regular"><?= $text ?></p>
                <?php endif; ?>
            </div>
        </div>

        <?php if($building_icon): ?>
            <div class="info-row__the-building">
                <img class="info-row__the-building-icon" src="<?= get_theme_root_uri() ?>/thegrove/src/images/svg/House-icon.svg">
            </div>
        <?php endif; ?>
    </div>
</section>
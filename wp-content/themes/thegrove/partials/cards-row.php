<?php

    $the_grove = get_sub_field("the_grove_icon") ? get_sub_field("the_grove_icon") : "";

?>

<section class="cards-row" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
    <div class="wrapper-full">
    <?php if($the_grove): ?>
        <div class="cards-row__the-grove">
            <img class="cards-row__the-grove-icon opacity-zero" src="<?= (wp_is_mobile()) ? get_theme_root_uri()."/thegrove/src/images/svg/The-Grove-small.svg" : get_theme_root_uri()."/thegrove/src/images/svg/The-Grove.svg" ?>">
        </div>
    <?php endif; ?>
        <div class="cards-row__row d-flex d-flex-wrap">
        
        <?php

        while( have_rows('cards') ): the_row();

            if(get_sub_field('image')):
                $image = get_sub_field("image");
                $picture = $image["sizes"]["card-image"];
    
            endif;

            if(get_sub_field('image_mobile')):
                $mobile_image = get_sub_field("image_mobile");
                $mobile_picture = $mobile_image["url"];
    
            endif;
    
            $title = get_sub_field("title") ? get_sub_field("title") : "";
            $subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
            $link = get_sub_field("link") ? get_sub_field("link") : "";
            $background = get_sub_field("background_select");
            $link = get_sub_field("link");
        ?>

            <a href="<?= $link['url'] ?>" class="cards-row__box <?= $background ?>">
                <div class="cards-row__inner d-flex">
                    <div class="cards-row__image">
                        <img class="cards-row__img lazy" data-src="<?= (wp_is_mobile()) ? $mobile_picture : $picture ?>">
                    </div>

                    <div class="cards-row__content">
                        <h2 class="cards-row__title text-header"><?= $title; ?></h2>
                        
                        <?php if($subtitle): ?>
                            <p class="cards-row__subtitle text-regular mt-2"><?= $subtitle; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </a>

        <?php 
        endwhile;
        ?>

        </div>
    </div>
</section>
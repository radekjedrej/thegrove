<?php
$floor_plan_repeater = !empty(get_sub_field("floor_plan_repeater")) ? get_sub_field("floor_plan_repeater") : "";
$accordion_title = !empty(get_sub_field("accordion_title")) ? get_sub_field("accordion_title") : "";
?>
<?php if ($floor_plan_repeater) : ?>
<section class="accordion__container" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
  <div class="accordion__wrapper wrapper-small">
    <div class="accordion-main__title d-flex">
      <h1 class="text-header"><?php echo $accordion_title ?></h1>
    </div>
    <ul class="accordion">
    <?php   
    if (have_rows("floor_plan_repeater")) :
      while (have_rows("floor_plan_repeater")) : the_row();  
      $name = !empty(get_sub_field("name")) ? get_sub_field("name") : "";
    ?>
    
    <li class="accordion__li text-regular">
      <div class="accordion__li__content">
        <div class="accordion__li__margin">
          <?php echo $name ?>
          <!-- Styles in btn-plus -->
          <button class="heavy__plus heavy__plus--auto more more3"><div class="screen-reader-text sr-only sr-only-focusable"></div></button>
        </div>
      </div>
      <ul class="accordion__children">
      <?php   
      if (have_rows("floor_plan_details_repeater")) :
        while (have_rows("floor_plan_details_repeater")) : the_row();  
        $room = !empty(get_sub_field("room")) ? get_sub_field("room") : "";
        $bedrooms = !empty(get_sub_field("number_of_bedrooms")) ? get_sub_field("number_of_bedrooms") : "";
        $bathrooms = !empty(get_sub_field("number_of_bathrooms")) ? get_sub_field("number_of_bathrooms") : "";    
        $sq_ft = !empty(get_sub_field("sq_ft")) ? get_sub_field("sq_ft") : "";
        $facilities = !empty(get_sub_field("facilities")) ? get_sub_field("facilities") : "";   
        $floors = !empty(get_sub_field("number_of_floors")) ? get_sub_field("number_of_floors") : "";
        $download_pdf_title = !empty(get_sub_field("download_and_view_pdf_title")) ? get_sub_field("download_and_view_pdf_title") : "";
        $download_pdf = !empty(get_sub_field("download_and_view_pdf")) ? get_sub_field("download_and_view_pdf") : "";   

      ?>
        <li class="accordion__children__li">
          <div class="accordion__children__li--margin">

            <div class="accordion__left">
              <div class="accordion__number">
                <?php if($room): ?>
                  <?php echo $room ?>
                <?php endif; ?>
              </div>
              <div class="accordion__inner">
                <div class="accordion__details">

                  <?php if($bedrooms): ?>
                    <div><?php echo $bedrooms ?></div>
                  <?php endif; ?>

                  <?php if($bathrooms): ?>
                    <div><?php echo $bathrooms ?></div>    
                  <?php endif; ?>

                  <?php if($sq_ft): ?>
                    <div><?php echo $sq_ft ?></div>
                  <?php endif; ?>

                  <?php if($facilities): ?>
                    <div><?php echo $facilities ?></div>    
                  <?php endif; ?>

                  <?php if($floors): ?>
                    <div><?php echo $floors ?></div>    
                  <?php endif; ?>
                  
                </div>
                <div class="accordion__view">
                  <?php if($download_pdf): ?>
                    <div class="accordion__link">
                      <a target="_blank" href="<?php echo $download_pdf['url'] ?>"><?= $download_pdf_title ?></a>
                    </div>
                </div>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </li>
        <?php
          endwhile;
        endif; 
        ?>
        <!-- REPEATER END -->
      </ul>
    </li>
    <?php
      endwhile;
    endif; 
    ?>
    <!-- REPEATER END -->

    </ul>
  </div>
</section>
<?php endif; ?> 
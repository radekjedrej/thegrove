<?php

    if(get_sub_field("image")):
        
        $image = get_sub_field("image");
        $picture = $image["sizes"]["banner-image"];

    endif;

    if(get_sub_field("image_mobile")):
        
        $mobile_image = get_sub_field("image_mobile");
        $mobile_picture = $mobile_image["url"];

    endif;

    $background = get_sub_field("background_select");
    $hidden = get_sub_field("hidden");
    
?>

<section class="big-image-row <?= $background; ?> <?= $hidden ?> wrapper-stretched" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
    <div class="wrapper-xfull-retina big-image-row__container">
        <div class="big-image-row__image d-flex">
            <img class="big-image-row__img img-fluid lazy" data-src="<?= (wp_is_mobile()) ? $mobile_picture : $picture ?>">
        </div>
    </div>
</section>
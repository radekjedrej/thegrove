<?php

$title = !empty(get_sub_field("title")) ? get_sub_field("title") : "";
$subtitle = !empty(get_sub_field("subtitle")) ? get_sub_field("subtitle") : "";
$text = !empty(get_sub_field("text")) ? get_sub_field("text") : "";
$gallery_repeater = !empty(get_sub_field("gallery_repeater")) ? get_sub_field("gallery_repeater") : "";

?>

<?php //if ($gallery_repeater) : ?>
<section class="gallery" <?= !is_front_page() ? 'data-scroll-section' : '' ?>>
    <div class="gallery__container wrapper-full d-flex d-flex-wrap">
        <div class="gallery__text-box">
            <div class="gallery__content">
                <h1 class="gallery__title text-header mt-4"><?= $title ?></h1>
                <h2 class="gallery__subtitle text-regular text-subheader mt-2"><?= $subtitle ?></h2>
                <p class="gallery__text text-regular mt-4"><?= $text ?></p>
            </div>
        </div>
        <?php if (have_rows('gallery_repeater')): ?>
        <div class="gallery__gallery-images d-flex d-flex-wrap">
            <?php while (have_rows("gallery_repeater")) : the_row(); 
            
            $image = !empty(get_sub_field("image")) ? get_sub_field("image") : "";
            $caption = !empty(get_sub_field("caption")) ? get_sub_field("caption") : "";
            $picture = !empty($image['url']) ? $image['url'] : "";
            $image_mobile = !empty(get_sub_field("image_mobile")) ? get_sub_field("image_mobile") : "";
            $picture_mobile = !empty($image_mobile['url']) ? $image_mobile['url'] : "";
            $hide = !empty(get_sub_field("hidden")) ? get_sub_field("hidden") : "";
            $instagramColor = get_sub_field("instagram_icon_color_mobile");

            if(!empty(get_sub_field("instagram_url"))):
                $instagram = get_sub_field("instagram_url");
                $instagramLink = $instagram['url'];

            else:
                $instagramLink = "#";

            endif;
            
            ?>

            <div class="gallery__image-box <?= $hide ?>">
                <?php if($image OR $image_mobile): ?>
                    <div class="gallery__img-container">
                        <img class="gallery__img img-fluid lazy" data-src="<?=(!wp_is_mobile()) ? $picture : $picture_mobile ?>">
                        <a class="gallery__svg-link-inside-img" href="<?= $instagramLink ?>"><svg class="gallery__svg-inside-img" xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 24 24"><path xmlns="http://www.w3.org/2000/svg" d="M12.004 5.838a6.157 6.157 0 00-6.158 6.158 6.157 6.157 0 006.158 6.158 6.157 6.157 0 006.158-6.158 6.157 6.157 0 00-6.158-6.158zm0 10.155a3.996 3.996 0 113.997-3.997 3.995 3.995 0 01-3.997 3.997z" fill="<?= $instagramColor ?>" data-original="#000000"/><path xmlns="http://www.w3.org/2000/svg" d="M16.948.076c-2.208-.103-7.677-.098-9.887 0-1.942.091-3.655.56-5.036 1.941C-.283 4.325.012 7.435.012 11.996c0 4.668-.26 7.706 2.013 9.979 2.317 2.316 5.472 2.013 9.979 2.013 4.624 0 6.22.003 7.855-.63 2.223-.863 3.901-2.85 4.065-6.419.104-2.209.098-7.677 0-9.887-.198-4.213-2.459-6.768-6.976-6.976zm3.495 20.372c-1.513 1.513-3.612 1.378-8.468 1.378-5 0-7.005.074-8.468-1.393-1.685-1.677-1.38-4.37-1.38-8.453 0-5.525-.567-9.504 4.978-9.788 1.274-.045 1.649-.06 4.856-.06l.045.03c5.329 0 9.51-.558 9.761 4.986.057 1.265.07 1.645.07 4.847-.001 4.942.093 6.959-1.394 8.453z" fill="<?= $instagramColor ?>" data-original="#000000"/><circle xmlns="http://www.w3.org/2000/svg" cx="18.406" cy="5.595" r="1.439" fill="<?= $instagramColor ?>" data-original="#000000"/></svg></a>
                    </div>
                <?php endif; ?>

                <div class="gallery__description d-flex">
                    <?php if($caption): ?>
                        <p class="gallery__caption text-regular"><?= $caption ?></p>
                    <?php endif; ?>
                    <a class="galley__svg-link" href="<?= $instagramLink ?>"><svg class="gallery__svg" xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 24 24"><path xmlns="http://www.w3.org/2000/svg" d="M12.004 5.838a6.157 6.157 0 00-6.158 6.158 6.157 6.157 0 006.158 6.158 6.157 6.157 0 006.158-6.158 6.157 6.157 0 00-6.158-6.158zm0 10.155a3.996 3.996 0 113.997-3.997 3.995 3.995 0 01-3.997 3.997z" fill="#535d58" data-original="#000000"/><path xmlns="http://www.w3.org/2000/svg" d="M16.948.076c-2.208-.103-7.677-.098-9.887 0-1.942.091-3.655.56-5.036 1.941C-.283 4.325.012 7.435.012 11.996c0 4.668-.26 7.706 2.013 9.979 2.317 2.316 5.472 2.013 9.979 2.013 4.624 0 6.22.003 7.855-.63 2.223-.863 3.901-2.85 4.065-6.419.104-2.209.098-7.677 0-9.887-.198-4.213-2.459-6.768-6.976-6.976zm3.495 20.372c-1.513 1.513-3.612 1.378-8.468 1.378-5 0-7.005.074-8.468-1.393-1.685-1.677-1.38-4.37-1.38-8.453 0-5.525-.567-9.504 4.978-9.788 1.274-.045 1.649-.06 4.856-.06l.045.03c5.329 0 9.51-.558 9.761 4.986.057 1.265.07 1.645.07 4.847-.001 4.942.093 6.959-1.394 8.453z" fill="#535d58" data-original="#000000"/><circle xmlns="http://www.w3.org/2000/svg" cx="18.406" cy="5.595" r="1.439" fill="#535d58" data-original="#000000"/></svg></a>
                </div>
            </div>  
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php //endif; ?>